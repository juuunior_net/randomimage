namespace RandomImage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Votes",
                c => new
                    {
                        VoteId = c.Int(nullable: false, identity: true),
                        Photo_LargeSquareThumbnailUrl = c.String(),
                        Photo_LargeUrl = c.String(),
                        Photo_PhotoId = c.String(),
                        Photo_ThumbnailUrl = c.String(),
                        User_Name = c.String(),
                        User_UserId = c.String(),
                        IsLike = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VoteId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Votes");
        }
    }
}
