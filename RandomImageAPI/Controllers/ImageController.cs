﻿using System.Web.Http;
using RandomImage.Models;

namespace RandomImage.Controllers
{
    
    public class ImageController : ApiController
    {
        
        [HttpGet]
        public Vote Get()
        {
            PhotoInfo photoInf = Randomizer.GetPhoto();
            UserInfo userInfo = new UserInfo { Name = "anonymous", UserId = "1" };
            Vote v = new Vote { Photo = photoInf, User = userInfo };
            return v;
        }
        
        [HttpPost]
        public string Vote(Vote vote)
        {
            using (var rep = new VoteDAL())
            {
                rep.SaveVote(vote);
            }
            return vote.IsLike ? "Liked" : "Disliked";
        }

     
       
    }
}
