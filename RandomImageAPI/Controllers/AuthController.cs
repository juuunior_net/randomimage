﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using RandomImage.Models;

namespace RandomImage.Controllers
{
    public class AuthController : Controller
    {
        private const string AppId = "1787465338192564";
        private const string AppSecret = "a41c80ebc9a17a39981a064c045a9e1f";

        //Facerbook callback url (handler)
        string redirectUri = "http://localhost:3571/Auth/FacebookLoginCheck";

        public class FacebookToken
        {
            public string access_token;
            public string token_type;
            public string expires_in;
        }

        public class FacebookUser
        {
            public string name;
            public string id;
        }

        [HttpGet]
        public RedirectResult FacebookLogin()
        {
            return Redirect(string.Format("https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}", AppId, redirectUri));
        }

        [HttpGet]
        public object FacebookLoginInfo()
        {
            return new { appId = AppId, authUrl = string.Format("https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}", AppId, redirectUri) };
        }

       

        [HttpGet]
        public async Task<ActionResult> FacebookLoginCheck(string code)
        {
            string url = string.Format("https://graph.facebook.com/v2.5/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}",
                AppId, redirectUri, AppSecret, code);
            HttpClient hc = new HttpClient();
            HttpResponseMessage resp = await hc.GetAsync(url);
            FacebookToken ft = await resp.Content.ReadAsAsync<FacebookToken>();
            url = "https://graph.facebook.com/v2.5/me?access_token=" + ft.access_token;
            resp = await hc.GetAsync(url);
            string s = await resp.Content.ReadAsStringAsync();
            FacebookUser fbu = (new JavaScriptSerializer()).Deserialize<FacebookUser>(s);

            UserInfo userInfo = new UserInfo { Name = fbu.name, UserId = fbu.id };
            Vote v = new Vote { Photo = null, User = userInfo };


            var json = JsonConvert.SerializeObject(userInfo);
            var userCookie = new HttpCookie("user", json);
            userCookie.Expires.AddDays(1);
            HttpContext.Response.Cookies.Add(userCookie);

            return RedirectToAction("Index", "Home", v);
            
        }



        [HttpGet]
        //[System.Web.Mvc.Route("api/auth/logout")]
        public ActionResult Logout()
        {
            var userCookie = new HttpCookie("user", "");
            HttpContext.Response.Cookies.Add(userCookie);

            Vote v = new Vote { Photo = null, User = null };
            return RedirectToAction("Index", "Home", v);
        }

    }
}