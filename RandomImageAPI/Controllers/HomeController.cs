﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FlickrNet;
using Newtonsoft.Json;
using RandomImage.Models;
using RedirectResult = System.Web.Mvc.RedirectResult;
using RedirectToRouteResult = System.Web.Mvc.RedirectToRouteResult;

namespace RandomImage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            Models.PhotoInfo photoInf = Randomizer.GetPhoto();
            UserInfo userInfo = new UserInfo {Name = "Test", UserId = "1"};
            Vote v = new Vote {Photo = photoInf, User = userInfo};
            return View(v);
        }
        
        public ActionResult History(string id)
        {
            string userId = id;
            HttpCookie userInfoCookie = Request.Cookies.Get("user");

            if (userInfoCookie != null)
            {
                UserInfo user = (UserInfo)JsonConvert.DeserializeObject(userInfoCookie.Value, typeof(UserInfo));
                
                if(user!=null)
                    userId = user.UserId;
            }


            ViewBag.Title = "History Page";            
            List<Vote> lst = new List<Vote>();
            using (var rep = new VoteDAL())
            {
                lst = rep.GetVotes(userId);
            }
            return View("History", lst);                
        }


   

      

    }
}
