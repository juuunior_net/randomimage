﻿var myModule = angular.module('app', ['restangular']);

myModule.run(function($rootScope,Restangular) {
    var rootService = 'https://rnd.taxcom.ru/RandomImage/';
    //Restangular.setBaseUrl(rootService);


    $rootScope.imageurl=null;
    $rootScope.whenLoading = false;

    $rootScope.UserInfo = {
        UserId: 1,
        Name: 'anonymous'
        
};

    $rootScope.GetNewImage = function() {
        $rootScope.whenLoading = true;
        var getImageUrl = "api/image";

        Restangular.one(getImageUrl).get().then(function (resp) {
            $rootScope.imageurl = resp.Photo.LargeUrl;
            $('.current-image').unbind().load(function () {
                $rootScope.whenLoading = false;
                $rootScope.$apply();
            });
            $rootScope.Vote = resp;
            $rootScope.message = null;
            busy = false;
        });
    }

    $rootScope.GetNewImage();


    var busy = false;

    $rootScope.DoVote = function (isLike) {


        if (!busy) {
            busy = true;
            var postImageUrl = "api/image/vote";
            $rootScope.Vote.IsLike = isLike;
            $rootScope.Vote.User = $rootScope.UserInfo;

            Restangular.all(postImageUrl).post(JSON.stringify($rootScope.Vote)).then(function (resp) {

                $rootScope.message = resp;
                setTimeout($rootScope.GetNewImage, 1000);

            });
        }
    }

    
    try{
        $rootScope.UserInfo = JSON.parse(document.cookie.split('user')[1].slice(1));
    }
    catch(e)
    {
    }


});

