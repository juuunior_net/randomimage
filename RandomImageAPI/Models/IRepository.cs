﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace RandomImage.Models
{
    public interface IRepository
    {
        void SaveVote(Vote v);
        List<Vote> GetVotes(string id);

    }
}