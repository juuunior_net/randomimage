﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace RandomImage.Models
{
    public class Vote
    {
        public int VoteId { get; set; } 
        public PhotoInfo Photo { get; set; }
        public UserInfo User { get; set; }
        public bool IsLike { get; set; }
    }


    public class VoteContext : DbContext
    {
        public DbSet<Vote> Votes { get; set; } 
    }

}