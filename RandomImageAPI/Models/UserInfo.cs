﻿
namespace RandomImage.Models
{
    public class UserInfo
    {
        public string Name { get; set; }
        public string UserId { get; set; }
    }
}