﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RandomImage.Models
{
    public class PhotoInfo
    {
        public string LargeSquareThumbnailUrl { get; set; }
        public string LargeUrl { get; set; }
        public string PhotoId { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}