﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RandomImage.Models
{
    public class VoteDAL:IRepository, IDisposable
    {
        public void SaveVote(Vote v)
        {
            using (var db = new VoteContext())
            {
                db.Votes.Add(v);
                db.SaveChanges();
            }
        }

        public List<Vote> GetVotes(string id)
        {
            if (String.IsNullOrEmpty(id))
                id = "1"; //reserved for test anonymus scenario

            List<Vote> lst = new List<Vote>();
            using (var db = new VoteContext())
            {
                lst = db.Votes.Where(v => v.User.UserId == id).OrderByDescending(v=>v.VoteId).ToList();
            }
            return lst;
        }

        public void Dispose()
        {
            
        }
    }
}