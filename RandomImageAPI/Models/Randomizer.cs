﻿using System;
using FlickrNet;

namespace RandomImage.Models
{
    public class Randomizer
    {

        public static PhotoInfo GetPhoto()
        {
            const string myFlickrApiKey = "e6c196c0a47f3a94494c9b2fabfe764c";
            Flickr flickr = new Flickr(myFlickrApiKey);
            var options = new PhotoSearchOptions {Tags = "colorful", PerPage = 1000, Page = 1};
            PhotoCollection photos = flickr.PhotosSearch(options);
            Random r = new Random();
            int i = r.Next(0, photos.Count);
            var photo = photos[i];
            PhotoInfo photoInf = new PhotoInfo
            {
                LargeSquareThumbnailUrl = photo.LargeSquareThumbnailUrl,
                LargeUrl = photo.LargeUrl,
                PhotoId = photo.PhotoId,
                ThumbnailUrl = photo.ThumbnailUrl
            };
            return photoInf;
        }
    }
}